#!../../bin/linux-x86_64/tlfw102DemoApp

## You may have to change tlfw102DemoApp to something else
## everywhere it appears in this file

< envPaths

epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TLFW102)/db")
epicsEnvSet("PREFIX",   "FW102")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tlfw102DDemoApp.dbd"
tlfw102DemoApp_registerRecordDeviceDriver pdbbase

#var streamDebug 1

epicsEnvSet("SERIAL_PORT", "/dev/ttyUSB0")

# drvAsynSerialPortConfigure(port, ttyName, priority, noAutoConnect, noProcessEosIn)
drvAsynSerialPortConfigure("$(PREFIX)", "$(SERIAL_PORT)", 0, 0, 0)
asynSetOption("$(PREFIX)", 0, "baud",   "115200")
asynSetOption("$(PREFIX)", 0, "bits",   "8")
asynSetOption("$(PREFIX)", 0, "parity", "none")
asynSetOption("$(PREFIX)", 0, "stop",   "1")
asynSetOption("$(PREFIX)", 0, "clocal", "Y")
asynSetOption("$(PREFIX)", 0, "crtscts","N")

# handled by the Terminator = CR; in the proto file
#asynOctetSetInputEos( "$(PREFIX)", 0, "\r")
#asynOctetSetOutputEos("$(PREFIX)", 0, "\r")

asynSetTraceIOMask("$(PREFIX)",0,0xff)
#asynSetTraceMask("$(PREFIX)",0,0xff)

# Load record instances
dbLoadRecords("$(FW102)/db/tlFW102.template","P=$(PREFIX):,R=,PORT=$(PREFIX)")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(PREFIX):,R=asyn,PORT=$(PREFIX),ADDR=0,OMAX=100,IMAX=100")

# For Autosave, before iocInit is called
#set_requestfile_path(".")
#set_savefile_path("./autosave")
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")

cd "${TOP}/iocBoot/${IOC}"
iocInit

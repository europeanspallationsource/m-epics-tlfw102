# Thorlabs filter wheel FW102C

More info at https://www.thorlabs.com/thorproduct.cfm?partnumber=FW102C.

# Prerequisites

Before IOC can be started the _/dev/ttyUSBx_ needs to have proper permissions; for this the udev rules file _88-thorlabs-fw102.rules_ can be copied to the _/etc/udev/rules.d_ folder.

# Usage

Check the _FW102C-Manual.pdf_ chapter 5.2.3 for details about the commands.

Here is a list of all PVs (by default $(P) macro is set to 'FW102'):

| PV | Description |
| ------------- :| ----- :|
| FW102:IDN | Identification string with model number and firmware version |
| FW102:BAUD | Sets the baud rate to 9600 or 115200 |
| FW102:BAUD_RBV | Reports used baud rate |
| FW102:POS | Moves the wheel to desired filter position |
| FW102:POS_RBV | Reports wheel position |
| FW102:SENSORS | Sensor control: on or off |
| FW102:SENSORS_RBV | Reports sensor mode |
| FW102:PCOUNT | Sets wheel type (6 or 12 positions) |
| FW102:PCOUNT_RBV | Reports wheel type |
| FW102:TRIG | Sets external trigger mode (input or output) |
| FW102:TRIG_RBV | Reports trigger mode |
| FW102:SPEED | Sets move profile mode (slow or fast) |
| FW102:SPEED_RBV | Reports move profile mode |
| FW102:SAVE | Save settings as default after power up |
| FW102:asyn | asynDriver specific PV |


Check if the device is present:

    caget -S FW102:IDN
    FW102:IDN THORLABS FW102C/FW212C Filter Wheel version 1.07

Select the wheel position 1 .. 6 with _FW102:POS_ PV:

    caput FW102:POS 1
    caput FW102:POS 3
    caput FW102:POS 4
    caput FW102:POS 2
    caput FW102:POS 6
    caput FW102:POS 5

The wheel should move to the selected position.

Check the wheel type:

    caget FW102:PCOUNT_RBV
    FW102:PCOUNT_RBV               6

Check trigger mode:

    caget FW102:TRIG_RBV
    FW102:TRIG_RBV                 Input

# Misc

When connected via USB a _/dev/ttyUSBx_ device node is created:

    [1205678.654688] usb 4-5: new full-speed USB device number 81 using xhci_hcd
    [1205678.789661] usb 4-5: New USB device found, idVendor=0403, idProduct=6001
    [1205678.789665] usb 4-5: New USB device strings: Mfr=1, Product=2, SerialNumber=3
    [1205678.789667] usb 4-5: Product: FW102C FILTER WHEEL
    [1205678.789669] usb 4-5: Manufacturer: Thorlabs
    [1205678.789670] usb 4-5: SerialNumber: 150623-03592
    [1205680.333294] usbcore: registered new interface driver ftdi_sio
    [1205680.333308] usbserial: USB Serial support registered for FTDI USB Serial Device
    [1205680.333424] ftdi_sio 4-5:1.0: FTDI USB Serial Device converter detected
    [1205680.333449] usb 4-5: Detected FT232RL
    [1205680.333608] usb 4-5: FTDI USB Serial Device converter now attached to ttyUSB0
